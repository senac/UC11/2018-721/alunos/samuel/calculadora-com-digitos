package br.senac.es.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {



    private TextView textViewDTextView;
    private static final String zero = "0";
    private static final String ponto = ".";
    private Button button0;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;
    private Button buttonSoma;
    private Button buttonSubtracao;
    private Button buttonDivisao;
    private Button buttonMultiplicacao;
    private Button buttonPonto;
    private Button buttonLimpar;
    private Button buttonIgual;
    private TextView Display;
    private boolean limpar;
    private double operador1;
    private double operador2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        limpar = false;

        initComponentes();


        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        buttonLimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limparDisplay();
            }
        });

        buttonPonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });


        buttonSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calcular();


            }
        });

        buttonIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
            }
        });

    }


    private void calcular() {

        String numero = Display.getText().toString();

        if (operador1 == 0) {
            operador1 = Double.parseDouble(numero);
            Log.i("#calculadora", "Numero1:" + operador1);
        } else {
            operador2 = Double.parseDouble(numero);
            Log.i("#calculadora", "Numero2:" + operador2);
        }
        limpar = true;

        if(operador1 != 0 && operador2 != 0) {
            if(operador2!=0) {
                operador1 = operador1 + operador2;
            }
            numero = String.valueOf(operador1);
            Display.setText(numero);
            operador2 = 0;
        }


    }

    private void limparDisplay() {
        this.Display.setText(zero);
        this.operador1 = 0;
        this.operador2 = 0;
    }


    private void escreverDisplay(View view) {

        Button button = (Button) view;
        String digito = button.getText().toString();
        String valorDisplay = Display.getText().toString();

        if (valorDisplay.equals(zero) || limpar) {
            limpar = false;
            if (!digito.equals(ponto)) {
                Display.setText(digito);
            } else {
                if (!valorDisplay.contains(ponto)) {
                    valorDisplay += ponto;
                    Display.setText(valorDisplay);
                }
            }
        } else {

            valorDisplay += digito;
            Display.setText(valorDisplay);

        }


    }

    private void initComponentes() {
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        buttonSoma = (Button) findViewById(R.id.buttonSoma);
        buttonSubtracao = (Button) findViewById(R.id.buttonSubtracao);
        buttonDivisao = (Button) findViewById(R.id.buttonDivisao);
        buttonMultiplicacao = (Button) findViewById(R.id.buttonMultiplicacao);
        buttonPonto = (Button) findViewById(R.id.buttonPonto);
        buttonLimpar = (Button) findViewById(R.id.buttonLimpar);
        buttonIgual = (Button) findViewById(R.id.buttonIgual);
        Display = (TextView) findViewById(R.id.display);
    }
}









